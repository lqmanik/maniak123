#ifndef _MY_H_
#define _MY_H_

#include <ncurses.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>

void map_key(int key, int pos[2], char **str);
int find_player_pos(char *spot);
int move_up(int pos[2], char **str);
int move_down(int pos[2], char **str);
int move_left(int pos[2], char **str);
int move_right(int pos[2], char **str);
int my_sokoban(char const *map);
int get_size(char const *filepath);



typedef struct s_pos {
    int x;
    int y;
} t_pos;

typedef struct s_map {
    char **map;
    char *path;
    int line;
    int col;
} t_map;

#endif /* _MY_H_ */