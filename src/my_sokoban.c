#include "my.h"

int my_sokoban(char const *map)
{
    char *str = 0;
    int key = 0;
    int pos[2];

    initscr();
    curs_set(0);
    noecho();
    keypad(stdscr, true);
    printw(map);
    refresh();
    while (1) {
        find_player_pos(str);
        map_key(key, pos, str);
    }
    refresh();
    endwin();
    return (0);
}