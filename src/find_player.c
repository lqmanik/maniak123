#include "../include/my.h"

int find_player_pos(char *spot)
{
    int i = 0;
    int j = 0;

    while (1) {
        for (; spot[i] != 'P'; i++) {
            j = i;
            i++;
            return j;
        }
        i = 0;
    }
}