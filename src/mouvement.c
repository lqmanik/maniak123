#include "../include/my.h"

void map_key(int key, int pos[2], char **str)
{
    if (key == KEY_UP)
        move_up(pos, str);
    if (key == KEY_DOWN)
        move_down(pos, str);
    if (key == KEY_LEFT)
        move_down(pos, str);
    if (key == KEY_RIGHT)
        move_down(pos, str);
}

int move_up(int pos[2], char **str)
{
    if (str[pos[0] - 1][pos[1]] = 'X' && str[pos[0] - 2][pos[1]] != '#' &&
        str[pos[0] - 2][pos[1]] != 'X') {
        str[pos[0]][pos[1]] = ' ';
        str[pos[0] - 1][pos[1]] = 'P';
        str[pos[0] - 2][pos[1]] = 'X';
        }
}

int move_down(int pos[2], char **str)
{
    if (str[pos[0] + 1][pos[1]] == 'X' && str[pos[0] + 2][pos[1]] != '#' &&
        str[pos[0] + 2][pos[1]] != 'X') {
        str[pos[0]][pos[1]] = ' ';
        str[pos[0] + 1][pos[1]] = 'P';
        str[pos[0] + 2][pos[1]] = 'X';
        }
}

int move_left(int pos[2], char **str)
{
    if (str[pos[0]][pos[1] - 1] == 'X' && str[pos[0]][pos[1] - 2] != '#' &&
        str[pos[0]][pos[1] - 2] != 'X') {
        str[pos[0]][pos[1]] = ' ';
        str[pos[0]][pos[1] - 1] = 'P';
        str[pos[0]][pos[1] - 2] = 'X';
        }
}

int move_right(int pos[2], char **str)
{
    if (str[pos[0]][pos[1] + 1] == 'X' && str[pos[0]][pos[1] + 2] != '#' &&
        str[pos[0]][pos[1] + 2] != 'X') {
        str[pos[0]][pos[1]] = ' ';
        str[pos[0]][pos[1] + 1] = 'P';
        str[pos[0]][pos[1] + 2] = 'X';
        }
}