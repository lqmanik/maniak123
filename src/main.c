#include "../include/my.h"

//int get_size(char const *filepath);

int get_size(char const *filepath)
{
    struct stat str;

    if (stat(filepath, &str) != -1)
        return str.st_size;
    else
        return 0;
}

int main(int ac, char **av)
{
    int file;
    size_t map_size = get_size(av[1]);
    char *buffer;

    if ((ac != 2) || map_size == 0) {
        write(2, "USAGE: ./sokoban map\n", 21);
        return (84);
    }
    file = open(av[1], O_RDONLY);
    buffer = malloc(sizeof(char) * map_size + 2);

    if (file == -1) {
        write(2, "Sokoban cannot access the file\n", 31);
        close(file);
        return (84);
    } else {
        read(file, buffer, map_size);
        close(file);
        my_sokoban(buffer);
        return (file);
    }
}