SRC	=	./src/main.c		\
		./src/my_sokoban.c	\
		./src/find_player.c	\
		./src/mouvement.c

OBJ	=	$(SRC:.c=.o)

NAME	=	my_sokoban

CPPFLAGS	=	-I include/

CFLAGS	=	-g -Wall -Wextra

all: $(NAME)

$(NAME):	$(OBJ)
	@gcc $(CFLAGS) -o $(NAME) $(OBJ) $(CPPFLAGS) -lncurses

clean:
	@rm -f $(OBJ)

fclean:	clean
	@rm -f $(NAME)

re:	fclean all
